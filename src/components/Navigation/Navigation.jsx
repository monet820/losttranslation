import React from "react";
import "./Navigation.css";

import { useSelector, useDispatch } from "react-redux";
import {clearNameAction} from "../../store/actions/userActions";
import {logOutAction} from "../../store/actions/isLoggedActions";

import { Link } from "react-router-dom";

function NavigationComponent(props) {

  const username = useSelector(state => state.user);
  const dispatch = useDispatch();
  
  function clearUserName() {
    dispatch(clearNameAction());
    dispatch(logOutAction());
  }

  return (
    <div className="container">
    <nav className="navbar border-bottom border-warning navbar-expand-sm navbar-dark fixed-top background-color-main">
      <Link to="/landing"><span className="navbar-brand">Lost in Translation</span></Link>

      <div className="collapse navbar-collapse" id="navbarText">
        <ul className="navbar-nav mr-auto">

          <Link to="/translate">
            <li className="nav-item nav-link">Translation</li>
          </Link>

          <Link to="/profile">
            <li className="nav-item nav-link">Profile</li>
          </Link>
          
        </ul>

       <div>
         {username != "" ? <button onClick={clearUserName} className="navbar-text bg-dark">Logout: {username}</button> : <p></p>}
       </div>
  
        

      </div>
    </nav>
    </div>
  )
}

export default NavigationComponent;