import React, { useState } from "react";
import "./Translate.css";
import { useDispatch } from "react-redux";

import { actionAddTranslation } from "../../store/actions/translationActions";

function TranslateComponent() {

    const dispatch = useDispatch();

    const [signTranslation, setSignTranslation] = useState([]);
    const [translationText, setTranslationText] = useState([]);

    const [warningText, setWarningText] = useState([]);


    // function to get text input.
    const checkText = function (event) {
        setTranslationText(event.target.value.toLowerCase());
    }

    function onSubmitClicked() {

        if (/[^a-zA-Z]/.test(translationText)) {
            setSignTranslation([]);
            setWarningText("Invalid input, only accept a-z")
        }
        else {
            addTranslationToHistory(translationText);
            setWarningText([]);
            setSignTranslation(translationText.split(""));
        }
    };

    function addTranslationToHistory(text) {
        if (text != null) {
            dispatch(actionAddTranslation(text));
        }
    }

    const signsTest = signTranslation.map(sign =>
        <img className="images" src={"/images/sign-symbols/" + sign + ".png"} alt={sign} />
    );

    return (
        <div >
            <div className="row justify-content-center translationInputContainer">
                <input onChange={checkText} type="text" id="translationInput" className="form-control" placeholder="Hello"></input>
            </div>

            <div className="translationOutputContainer container">
                
                <div className="TextBorder">
                    {warningText != "" ? <div className="border border-danger showSignsOutput"> {warningText} </div> : <div></div>}
                    {signsTest != "" ? <div className="showSignsOutput"> {signsTest} </div> : <div></div>}
                </div>

                <div className="buttonDiv">
                    <input onClick={onSubmitClicked} id="buttonSubmitTranslate" type="button" name="submit" className="btn" value="Translate"></input>
                </div>

            </div>
        </div>
    )
}

export default TranslateComponent;