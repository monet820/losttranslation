import React from "react";

import { useDispatch, useSelector } from "react-redux";
import {clearNameAction} from "../../store/actions/userActions";
import {logOutAction} from "../../store/actions/isLoggedActions";
import { actionClearTranslations } from "../../store/actions/translationActions";


import "./Profile.css";

function ProfileComponent() {

    const bodyStyling = {
        marginTop: "20px",
    }

    const historyTranslations = useSelector(state => state.translation);
    const dispatch = useDispatch();

    function clearUserName() {
        dispatch(clearNameAction());
        dispatch(logOutAction());
      }

      function clearTranslateHistory(){
        dispatch(actionClearTranslations());
      }

    return (
        <div style={bodyStyling} className="container">
            <h1 className="list-group-item">Previous 10 searches</h1>
            <ul className="list-group">
                {historyTranslations.map(name => (
                    <li  className="list-group-item">{name}</li>
                ))}
            </ul>

            <button onClick={clearTranslateHistory}>Clear History</button>
            <button onClick={clearUserName}>logout</button>
        </div>
    )
}

export default ProfileComponent;