import React from "react";
import "./Landing.css";

import { useDispatch, useSelector } from "react-redux";

import { setNameAction } from "../../store/actions/userActions";
import { logInAction } from "../../store/actions/isLoggedActions";

function LandingComponent() {

    const bodyStyling = {
        marginTop: 0,
        height: "100vh",
        background: "linear-gradient(to bottom, #E7B355 50%,white 50%)"
    };

    const dispatch = useDispatch();
    const username = useSelector(state => state.user);

    let inputName = null;

    // function to get text input.
    const checkText = function (event) {
        inputName = event.target.value;
    }

    // Function to login, and update store name and login.
    const login = function (event) {
        if (username === "" && inputName != null) {
            dispatch(setNameAction(inputName));
            dispatch(logInAction());
        }
    };

    return (
        <div style={bodyStyling} id="login">
            <div className="container ">
                <div className="row justify-content-center">
                    <div className="col-md-3">
                        <img className="logoPicture" src="images/logo/Logo.png" alt="logo picture" />
                    </div>
                    <div className="col-md-7 headerText">
                        <h1>Lost in Translation</h1>
                        <h2>Get started</h2>
                    </div>
                </div>
                <div id="login-row" className="row justify-content-center align-items-center">
                    <div id="login-column" className="col-md-6">
                        <div id="login-box" className="col-md-12">
                            <form id="login-form" className="form">
                                <div className="form-group">
                                    <input placeholder="What's your name?" onChange={checkText} type="text" name="username" id="username" className="form-control"></input>
                                </div>
                                <div className="form-group text-center">
                                    <input onClick={login} id="buttonSubmit" type="button" name="submit" className="btn btn-info" value="submit"></input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div >



    )

}


export default LandingComponent;