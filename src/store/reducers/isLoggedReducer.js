import { ACTION_SIGN_IN } from "../actions/isLoggedActions"
import { ACTION_SIGN_OUT } from "../actions/isLoggedActions"

export function isLoggedReducer(state = false, action) {
    switch (action.type) {
        case "ACTION_SIGN_IN":
            return state = true;
        case "ACTION_SIGN_OUT":
            return state = false;
        
        default:
            return state;
    }
}