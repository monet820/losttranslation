import { ACTION_ADD_TRANSLATION } from "../actions/translationActions";
import { ACTION_CLEAR_TRANSLATION } from "../actions/translationActions";

export function translationReducer(state = [], action) {
    switch (action.type) {

        case "ACTION_CLEAR_TRANSLATION":
            return state = [];

        case "ACTION_ADD_TRANSLATION":
            if (state.length > 9) {

                return [...state.slice(1), action.payload];
            } else {
                return [...state, action.payload];
            }

        default:
            return state;
    }
}