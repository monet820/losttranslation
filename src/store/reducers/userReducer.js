import { ACTION_SET_USERNAME } from "../actions/userActions"
import { ACTION_CLEAR_USERNAME } from "../actions/userActions"

export function userReducer(state = "", action) {
    switch (action.type) {
        case "ACTION_SET_USERNAME":
            return [...state, ...action.payload];
        case "ACTION_CLEAR_USERNAME":
            return state = "";

        default:
            return state;
    }
};
