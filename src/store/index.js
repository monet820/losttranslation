import { createStore, combineReducers } from "redux";

import { userReducer } from "./reducers/userReducer";
import { isLoggedReducer } from "./reducers/isLoggedReducer";
import { translationReducer } from "./reducers/translationReducer";


function saveToLocalStorage(state) {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem("state", serializedState);
    }
    catch (e) {
        console.log(e);
    }
}

function loadFromLocalStorage() {
    try {
        const serializedState = localStorage.getItem("state");
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (error) {
        console.log(error);
        return undefined;
    }
}

const rootReducers = combineReducers({
    user: userReducer,
    islogged: isLoggedReducer,
    translation: translationReducer,
});

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducers,
    persistedState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

store.subscribe(() => saveToLocalStorage(store.getState()))

export default store;