export const ACTION_SET_USERNAME = "ACTION_SET_USERNAME";
export const ACTION_CLEAR_USERNAME = "ACTION_CLEAR_USERNAME";

export const setNameAction = (name = []) => ({
    type: ACTION_SET_USERNAME,
    payload: name
});

export const clearNameAction = () => ({
    type: ACTION_CLEAR_USERNAME
});
