export const ACTION_SIGN_IN = "ACTION_SIGN_IN";
export const ACTION_SIGN_OUT = "ACTION_SIGN_OUT";

export const logInAction = () => ({
    type: ACTION_SIGN_IN,
});

export const logOutAction = () => ({
    type: ACTION_SIGN_OUT,
});
