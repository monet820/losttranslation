export const ACTION_GET_HISTORY = "ACTION_GET_HISTORY";
export const ACTION_ADD_TRANSLATION = "ACTION_ADD_TRANSLATION";
export const ACTION_CLEAR_TRANSLATION = "ACTION_CLEAR_TRANSLATION";


export const actionGetHistory = () => ({
    type: ACTION_GET_HISTORY
});

export const actionAddTranslation = (name = []) => ({
    type: ACTION_ADD_TRANSLATION,
    payload: name
});

export const actionClearTranslations = () => ({
    type: ACTION_CLEAR_TRANSLATION
});
