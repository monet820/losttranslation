import React from "react";
import LandingComponent from "../components/Landing/Landing";

function Landing() {

    return (
        <main>
           <LandingComponent></LandingComponent>
        </main >
    );
}

export default Landing;