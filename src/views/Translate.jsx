import React from "react";
import TranslateComponent from "../components/Translate/Translate";

function Translate(){
    return(
        <main>
            <TranslateComponent></TranslateComponent>
        </main>
    );
}

export default Translate;