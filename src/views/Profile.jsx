import React from "react";
import ProfileComponent from "../components/Profile/Profile";

function Profile() {
    return (
        <main>
            <ProfileComponent></ProfileComponent>
        </main>
    );
}

export default Profile;