import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

// Importing Views
import Profile from "./views/Profile";
import Landing from "./views/Landing";
import Translate from "./views/Translate";


// Importing component
import NavigationComponent from './components/Navigation/Navigation';

// Importing useSelector to get state of 
import { useSelector } from "react-redux";

function App() {

  const checkLogin = useSelector(state => state.islogged);

  function PrivateRoute({ component: Component, authed, ...rest }) {
    return (
      <Route
        {...rest}
        render={(props) => authed === true
          ? <Component {...props} />
          : <Redirect to={{ pathname: '/landing', state: { from: props.location } }} />}
      />
    )
  }

  return (
    <Router>
      <div className="App">

        <div className="navSize">
          <NavigationComponent></NavigationComponent>
        </div>

        <Switch>

          <Route path="/landing">
            {checkLogin ? <Redirect to="/translate" /> : <Landing />}
          </Route>

          <PrivateRoute authed={checkLogin} path="/translate" component={Translate} />
          <PrivateRoute authed={checkLogin} path="/profile" component={Profile} />


          <Route exact path="/">
            <Redirect to="/landing"></Redirect>
          </Route>

          <Route path="*" component={Landing} />

        </Switch>
      </div>
    </Router>
  );
}

export default App;
